# show variable when run ci/cd
set -x

DockerHubUrl="localhost:5000"
ProjectName=$1
Tag=$2
ImageName=""
Ip=""
StackName="$1_stack"

if [ -z "$1" ]; then
    echo "ProjectName NOT ALLOW empty"
    exit
fi	

if [ -z "$2" ]; then
    echo "Tag NOT ALLOW empty"
    exit
fi

ImageName="$DockerHubUrl/prometheus_test:${Tag}"

# execute as a subcommand in order to avoid the variables remain set
(
	export ProjectName=$ProjectName
	export ImageName=$ImageName
		
	docker image pull "$ImageName"

  # load image to k8s image
  kind load docker-image "$ImageName" --name myk8s
)
